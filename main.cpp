#include "main.hpp"

using namespace std;
using namespace cv;

#define MAX_SIZE 5

#define RED Scalar(0,0,255)
#define BLUE Scalar(255,0,0)
#define GREEN Scalar(0,255,0)
#define BLACK Scalar(255,255,255)
#define WHITE Scalar(0,0,0)
#define GRAY Scalar(127,127,127)
#define PINK Scalar(127,0,255)
#define YELLOW Scalar(0,246,255)
#define ORANGE Scalar(0,56,255)

#define NUM_OF_DETECTORS 3

vector<string> names = {"SURF", "SIFT", "ORB"};


int main(int argc, char **argv)
{
    if(argc < 2){
        cout << argv[0] << " path_to_file.jpg" << endl;
        return EXIT_SUCCESS;
    }
    // Need to init nonfree module!
    initModule_nonfree();

    cout << "Getting file: " << argv[1] << endl;

    Mat input_img_gray, input_img_color;

    CV_PROFILE("Loading gray", input_img_gray = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE););
    CV_PROFILE("Loading color", input_img_color = imread(argv[1], CV_LOAD_IMAGE_UNCHANGED););

    if(!input_img_color.data || !input_img_gray.data){
        cerr << "Could not open the file" << endl;
        return EXIT_FAILURE;
    }

    namedWindow(WIN_1, WINDOW_NORMAL);
    imshow(WIN_1, input_img_color);

    int maxCorners = 1500;
    vector<Point2f> corners;
    vector<Point2f> kps[NUM_OF_DETECTORS];

    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03);
    Size subPixWinSize(10,10), winSize(31,31);


    CV_PROFILE("GFTT", goodFeaturesToTrack(input_img_gray, corners, maxCorners, 0.05, 20.0);
                       cornerSubPix(input_img_gray, corners, subPixWinSize, Size(-1,-1), termcrit);
    );

    SurfFeatureDetector surf_detector(400);
    SurfDescriptorExtractor surf_extractor;
    SiftFeatureDetector sift_detector;
    SiftDescriptorExtractor sift_extractor;
    ORB orb_detector;
    FREAK orb_extractor;

    vector<KeyPoint> tmp_k;
    Mat tmp_d;
    vector<vector<KeyPoint> > _keypoints;
    vector<Mat> _descriptors;

    CV_PROFILE("SURF",  surf_detector.detect(input_img_gray, tmp_k);
                        surf_extractor.compute(input_img_gray, tmp_k, tmp_d);
                        _keypoints.push_back(tmp_k); tmp_k.clear();
                        _descriptors.push_back(tmp_d); tmp_d = Mat();
    );
    CV_PROFILE("SIFT",  sift_detector.detect(input_img_gray, tmp_k);
                        sift_extractor.compute(input_img_gray, tmp_k, tmp_d);
                        _keypoints.push_back(tmp_k); tmp_k.clear();
                        _descriptors.push_back(tmp_d); tmp_d = Mat();
    );
    CV_PROFILE("ORB+FREAK",  orb_detector.detect(input_img_gray, tmp_k);
                        orb_extractor.compute(input_img_gray, tmp_k, tmp_d);
                        _keypoints.push_back(tmp_k); tmp_k.clear();
                        _descriptors.push_back(tmp_d); tmp_d = Mat();
    );

    Mat out(input_img_color);

    for(Point2f i : corners){
        circle(out, i, 5, BLUE, 1, 8);
    }
    unsigned int pos = 0;
    for(vector<KeyPoint> v: _keypoints){
        for(KeyPoint i : v){
           circle(out, i.pt, 5, RED, 0, 8);
           kps[pos].push_back(i.pt);
        }
        ++pos;
    }
    pos = 0;

    int number_of_points = 0;

    std::vector<MyData> datas[NUM_OF_DETECTORS];
    for(int i = 1; i <= MAX_SIZE; ++i){
        for (int j = 0; j < NUM_OF_DETECTORS; ++j) {
            MyData tmp = {i,0,0};
            datas[j].push_back(tmp);
        }
    }

    CV_PROFILE("Finding nearest",);
    {
        for(auto &corner: corners){ // for every corner find closest
            for(auto &vKp: kps){ // for every corner find closest
                //
                for(auto &kp: vKp){
                    double distance = DISTANCE(corner, kp);

                    for(int actual_max_size = 1; actual_max_size <= MAX_SIZE; ++actual_max_size){
                        if(distance < actual_max_size){ // its in range!

                            circle(out, kp, 5, PINK, 1, 8);
                            circle(out, corner, 5, PINK, 1, 8);
                            line(out, corner, kp, YELLOW);
                            ++number_of_points;

                            for(int i = actual_max_size - 1; i < MAX_SIZE; ++i){ // fill out all the others
                                datas[pos][i].numOfPoints += 1;
                                datas[pos][i].error += distance;
                            }

                        }
                    }
                }
                ++pos;
            }
            pos = 0;
        }
    }
//    cout << number_of_points << endl;
    for (pos = 0; pos < NUM_OF_DETECTORS; ++pos) {
        cout << names[pos]  << endl;
        for(int i = 0; i < MAX_SIZE; ++i){
            cout << "\t Closer than" << std::setw(4) << datas[pos][i].maxDistance << "px : " << std::setw(5) << datas[pos][i].numOfPoints << " : " << (datas[pos][i].error / datas[pos][i].numOfPoints) << endl;
        }
        cout << endl;
    }



    imshow(WIN_1, out);
    cvWaitKey();


    return EXIT_SUCCESS;
}

