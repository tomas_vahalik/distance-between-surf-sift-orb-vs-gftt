#ifndef MAIN_HPP
#define MAIN_HPP

#include <iostream>
#include <iomanip>
#include <utility>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/nonfree/nonfree.hpp>


#define USE_PROFILING true

#ifdef USE_PROFILING
#define CV_PROFILE(msg,code)	{\
    double __time_in_ticks = (double)cv::getTickCount();\
    { code }\
    std::cout << "\t" << std::setw(30) << msg  << "\t" << ((double)cv::getTickCount() - __time_in_ticks)/cv::getTickFrequency() << "s" << std::endl;\
}
#else
#define CV_PROFILE(msg,code) code
#endif


#define WIN_1 "window 1"

#define DISTANCE(a,b) sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2))

struct MyData {
    int maxDistance; // 1px?
    int numOfPoints; // how many can see?
    double error; // error
};


#endif // MAIN_HPP
